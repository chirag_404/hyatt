﻿using Hyatt.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Hyatt.Database
{
	public class HyattContext:DbContext,IDisposable
	{
		public DbSet<Menu> menus { get; set; }
		public DbSet<Category> categories { get; set; }
		public DbSet<SubCategory> subCategories { get; set; }
		public DbSet<Item> items { get; set; }
	}
}
