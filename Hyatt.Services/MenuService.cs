﻿using Hyatt.Database;
using Hyatt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hyatt.Services
{
	public class MenuService
	{
		public Menu GetMenu(int menuId)
		{

			using (HyattContext context = new HyattContext())
			{
				return context.menus.Find(menuId);
			}
		}

		public List<Menu> GetMenus()
		{

			using (HyattContext context = new HyattContext())
			{
				return context.menus.ToList();
			}
		}


		public void SaveMenu(Menu menu)
		{

			using (HyattContext context = new HyattContext())
			{
				context.menus.Add(menu);
				context.SaveChanges();
			}
		}

		public void UpdateMenu(Menu menu)
		{

			using (HyattContext context = new HyattContext())
			{
				//context.menus.Add(menu);
				context.Entry(menu).State = System.Data.Entity.EntityState.Modified;
				context.SaveChanges();
			}
		}

		public void DeleteMenu(int menuId)
		{

			using (HyattContext context = new HyattContext())
			{
				//context.menus.Add(menu);
				//context.Entry(menu).State = System.Data.Entity.EntityState.Modified;
				var menu = context.menus.Find(menuId);
				context.menus.Remove(menu);
				context.SaveChanges();
			}
		}

	}
}
