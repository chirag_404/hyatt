﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hyatt.Entity
{
	public class SubCategory: BaseEntity
	{
		public List<Item> items { get; set; }
		public Category category { get; set; }

	}
}
