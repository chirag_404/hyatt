﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hyatt.Entity
{
	public class Category : BaseEntity
	{

		public List<SubCategory> subCategories { get; set; }
		public Menu menu { get; set; }
	}
}
