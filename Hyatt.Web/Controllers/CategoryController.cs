﻿using Hyatt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Hyatt.Web.Controllers
{
    public class CategoryController : Controller
	{
		static List<Category> categories = new List<Category>();

		[HttpGet]
		public List<Category> GetAllMenus()
		{
			return categories;
		}

		[HttpGet]
		public Category GetCategoryById(int categoryId)
		{
			for (int i = 0; i < categories.Count; i++)
			{
				if (categoryId == categories[i].id)
				{
					return categories[i];
					//break;
				}
			}
			return null;

		}

		[HttpGet]
		public List<Category> GetCategoryByMenuId(int menuId)
		{
			for (int i = 0; i < categories.Count; i++)
			{
				//if (menuId == categories[i].(X =>X.id==menuId))
				
				//{
				//	return categories[i];
				//	//break;
				//}
			}
			return null;

		}

		[HttpPost]
		public List<Category> InsertMenu(Category category)
		{
			categories.Add(category);
			return categories;

		}

		[HttpPut]
		public List<Category> UpdateMenu(Category category)
		{
			categories.Add(category);
			return categories;
		}

		[HttpDelete]
		public List<Category> DeleteMenu(int categoryId)
		{
			for (int i = 0; i < categories.Count; i++)
			{
				if (categoryId == categories[i].id)
				{
					categories[i].active=false;
				}
			}


			return categories;
		}
	}
}