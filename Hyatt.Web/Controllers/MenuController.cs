﻿using Hyatt.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using Hyatt.Services;

namespace Hyatt.Web.Controllers
{
    public class MenuController : Controller
	{
		MenuService menuService = new MenuService();


		[HttpGet]
		public ActionResult Index()
		{
			var menus = menuService.GetMenus();
			return View(menus);
		}

		[HttpGet]
			  public ActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Create(Menu menu)
		{
			menuService.SaveMenu(menu);

			return RedirectToAction("index");
		}


		[HttpGet]
		public ActionResult Edit(int menuId)
		{
			var menu = menuService.GetMenu(menuId);

			return View(menu);
		}

		[HttpPost]
		public ActionResult Edit(Menu menu)
		{
			menuService.UpdateMenu(menu);

			return RedirectToAction("index");
		}

		[HttpGet]
		public ActionResult Delete(int menuId)
		{
			var menu = menuService.GetMenu(menuId);

			return View(menu);
		}

		[HttpPost]
		public ActionResult Delete(Menu menu)
		{
			//menu = menuService.GetMenu(menu.id);
			menuService.DeleteMenu(menu.id);

			return RedirectToAction("index");
		}

	}
}