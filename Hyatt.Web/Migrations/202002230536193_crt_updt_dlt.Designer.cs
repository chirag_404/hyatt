// <auto-generated />
namespace Hyatt.Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class crt_updt_dlt : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(crt_updt_dlt));
        
        string IMigrationMetadata.Id
        {
            get { return "202002230536193_crt_updt_dlt"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
