﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hyatt.Web.Startup))]
namespace Hyatt.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
